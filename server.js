"use strict";
var express = require('express'),
    fs = require('fs'),
    osmosis = require('osmosis'),
    xlsx = require('xlsx'),
    bodyParser = require('body-parser'),
    IP = process.env.IP || process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1',
    PORT = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 3000,
    app = express();
app.use(bodyParser.urlencoded({
    extended:true
}));
app.use(express.static('public'));
app.use(bodyParser.json());

//multer's disk storage settings

//excel upload from form

app.post('/ebay', function(req, res, next){
    console.log('Requesting');
    var links = req.body.links;
    var stack = [];
    for(let i = 0, j=1000; i<links.length; i++,j=j+1000){
        setTimeout(function(){
            osmosis.get(links[i])
                .set({
                    'name': '.it-ttl',
                    'price':'#prcIsum',
                    'ended':'span#w1-3-_msg'
                })
                .data(function(results){
                    var a = results.name;
                    results.name = a.replace(a.match(/Details about   /)[0], ''); //Removing Details about..
                    results.price = +results.price.match(/[0-9]*\.[0-9]*/); //removing US $
                    console.log(results);
                    stack.push(results);
                })
                .done(function(){
                    if(stack.length === links.length) //hack to make sure we have the complete list
                        try{
                            res.send(stack);
                        }catch(e){
                            console.error(er);
                        }
                })
        },j);
    }
});





app.get('/', function(req, res, next){
    res.send('index.html');
})
app.listen(process.env.PORT, process.env.IP, function(){
    console.log('Listening at',PORT, "on IP",IP);
});