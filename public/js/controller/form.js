angular.module('kingy')
    .controller('formCtrl', ['$scope', '$http', '$state','ListService', function ($scope, $http, $state, ListService) {
        $scope.loading=false;
        $scope.status="";
        console.log('inside form');
        console.log(ListService.getList());
        $scope.upload = function(rawLinks){
            $scope.loading=true;
            $scope.status="Ruk. Processing...";
            $http({
                method: 'POST',
                url:'/ebay',
                data: {
                    links: rawLinks.split('\n')
                }
            }).then(function(res){
                $scope.loading=false;
                ListService.updateList(res.data);
                $state.go('result');
            }, function(res){
                $scope.status="Shit went down.";
                console.error(res);
                $scope.loading=false;

            })
        }

    }])