angular.module('kingy', ['ui.router', 'ngFileUpload'])
    .config(function($stateProvider, $urlRouterProvider){
        $urlRouterProvider.otherwise('/');
        $stateProvider
            .state('home', {
                url:'/',
                templateUrl:'/partial/form.html',
                controller:'formCtrl'
            })
            .state('result', {
                url:'/result',
                templateUrl:'/partial/result.html',
                controller:'resultCtrl'
            })
    })
